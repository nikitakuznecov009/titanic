import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title']=df['Name'].str.extract(' ([A-Za-z]+)\.', expand=False)
    titles=['Mr.','Mrs.','Miss.']
    result=[]
    for title in titles:
        filt_data=df[df['Title']==title] &( ~df['Age'].isnull())
        age=filt_data['Age'].median()
        missing_val=df[(df['Title']==title) & (df['Age'].isnull())]['Age'].count()
        result.append((title, missing_val,age))
    return(result)
print(get_filled())
