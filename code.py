import pandas as pd
titanic['Title']=titanic['Name'].str.extract(' ([A-Za-z]+)\.', expand=False)
titles=['Mr.','Mrs.','Miss.']
result=[]
for tile in titles:
    filt_data=titanic(titanic['Title']==title) & ( ~titanic['Age'].isnull())
    age=filt_data['Age'].median()
    missing_val=titanic[titanic['Title']==title] & (titanic['Age'].isnull())['Age'].count()
    result.append((title, missing_val,age))
print(result)